﻿using SwaggerAuth.TokenProvider;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.IO;
using SwaggerAuth.Model;
using Microsoft.AspNetCore.Http;

namespace SwaggerAuth.Utils
{
    public class GetLoginToken
    {
        public static LoginResponse Execute(ApplicationUser user, HttpContext context)
        {
            var now = DateTime.UtcNow;

            var options = GetOptions();
            var db = context.RequestServices.GetService(typeof(ApiDbContext));

            var refreshToken = new RefreshToken()
            {
                UserId = user.Id,
                Token = Guid.NewGuid().ToString("N"),
                IssuedUtc = now,
                ExpiresUtc = now.Add(options.Refresh)
            };
            ((ApiDbContext)db).InsertNew(refreshToken);
            ((ApiDbContext)db).SaveChanges();

            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName)
            };

            var jwt = new JwtSecurityToken(
                issuer: options.Issuer,
                audience: options.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(options.Expiration),
                signingCredentials: options.SigningCredentials);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new LoginResponse
            {
                access_token = encodedJwt,
                expires_in = jwt.ValidTo,
                refresh_token = refreshToken.Token,
                refresh_token_expires_in = refreshToken.ExpiresUtc,
                userName = user.UserName
            };
            return response;
        }


        public static TokenProviderOptions GetOptions()
        {
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.Config.GetSection("TokenAuthentication:SecretKey").Value));

            return new TokenProviderOptions
            {
                Audience = Configuration.Config.GetSection("TokenAuthentication:Audience").Value,
                Issuer = Configuration.Config.GetSection("TokenAuthentication:Issuer").Value,
                Expiration = TimeSpan.FromDays(Convert.ToInt32(Configuration.Config.GetSection("TokenAuthentication:ExpirationDays").Value)),
                Refresh = TimeSpan.FromDays(Convert.ToInt32(Configuration.Config.GetSection("TokenAuthentication:RefreshDays").Value)),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
            };
        }

        public static class Configuration
        {
            public static IConfigurationRoot Config { get; set; }

            static Configuration()
            {
                var builder = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
                Config = builder.Build();

                Configuration.Config = Config;
            }

            public static string DbConnection => Config["DefaultConnection"];
        }
    }
}
