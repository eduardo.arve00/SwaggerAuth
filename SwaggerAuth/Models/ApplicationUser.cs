﻿using Microsoft.AspNetCore.Identity;

namespace SwaggerAuth
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
    }
}